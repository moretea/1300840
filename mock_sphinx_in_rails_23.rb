# Use this to stub sphinx like this:
# Product.stub(:search).and_return(MockSphinx::ProductStubber.new(products))
module MockSphinx
  class ProductStubber
    def initialize products
      @products = products
    end
   
    # Swallow all following scope calls
    def method_missing name, *args, &block
      self
    end
   
    # Gets called by rails
    def map
      @products.map{ |p| yield p}
    end
   
    # Return everything in one page
    def total_pages
      1
    end
   
    def count
      @products.length
    end
    alias :length :count
    
    
    # No, we're not empty :)
    def empty?
      false
    end
  end
end
